package com.morgan.apps.card.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.morgan.apps.card.dao.MotorDao;
import com.morgan.apps.card.model.Motor;
import com.morgan.apps.card.service.MotorService;

@Service("motorService")
public class MotorServiceImpl implements MotorService {

     private static final String LOAD_ALL_MOTORS_HQL = "FROM Motor";

     @Autowired
     private MotorDao motorDao;

     public MotorServiceImpl() {

     }

     @Override
     public List<Motor> loadAll() {

          Map<String, Object> parametersNamesAndValues = new HashMap<String, Object>();

          return this.motorDao.createQuery(LOAD_ALL_MOTORS_HQL,
                    parametersNamesAndValues);
     }

     @Override
     public void save(Motor motor) {
          this.motorDao.save(motor);

     }

     public MotorDao getMotorDao() {
          return motorDao;
     }

     public void setMotorDao(MotorDao motorDao) {
          this.motorDao = motorDao;
     }

}
