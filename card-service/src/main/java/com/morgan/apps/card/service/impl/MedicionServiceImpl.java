package com.morgan.apps.card.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.morgan.apps.card.dao.MedicionDao;
import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.service.MedicionService;

@Service("medicionService")
public class MedicionServiceImpl implements MedicionService {

     @Autowired
     private MedicionDao medicionDao;

     public MedicionServiceImpl() {

     }

     @Override
     @Transactional
     public void save(Medicion medicion) {

          this.medicionDao.save(medicion);

     }

     @Override
     public List<Medicion> loadInTimeFrame(Canal canal, Date inicio, Date fin) {

          Map<String, Object> parametersNamesAndValues = new HashMap<String, Object>();

          parametersNamesAndValues.put("idCanal", canal.getId());
          parametersNamesAndValues.put("fechaInicio", inicio);
          parametersNamesAndValues.put("fechaFin", fin);

          List<Medicion> results = this.medicionDao
                    .createQuery(
                              "FROM Medicion m WHERE m.canal.id = :idCanal"
                                        + " AND m.fecha BETWEEN :fechaInicio AND :fechaFin",
                              parametersNamesAndValues, 10);

          return results;

     }

     @Override
     public List<Medicion> loadLastTen() {

          Map<String, Object> parametersNamesAndValues = new HashMap<String, Object>();

          List<Medicion> results = this.medicionDao.createQuery(
                    "FROM Medicion ORDER BY fecha DESC",
                    parametersNamesAndValues, 10);

          return results;
     }

     public MedicionDao getMedicionDao() {
          return medicionDao;
     }

     public void setMedicionDao(MedicionDao medicionDao) {
          this.medicionDao = medicionDao;
     }

}
