package com.morgan.apps.card.service;

import java.util.Date;
import java.util.List;

import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;

public interface MedicionService {

     public void save(Medicion medicion);

     public List<Medicion> loadInTimeFrame(Canal canal, Date inicio, Date fin);

     public List<Medicion> loadLastTen();

}
