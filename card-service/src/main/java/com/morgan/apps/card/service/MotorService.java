package com.morgan.apps.card.service;

import java.util.List;

import com.morgan.apps.card.model.Motor;

public interface MotorService {

     public List<Motor> loadAll();

     public void save(Motor motor);

}
