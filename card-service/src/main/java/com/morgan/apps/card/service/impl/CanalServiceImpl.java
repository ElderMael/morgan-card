package com.morgan.apps.card.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.morgan.apps.card.dao.CanalDao;
import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.service.CanalService;

@Service("canalService")
public class CanalServiceImpl implements CanalService {

	@Autowired
	private CanalDao canalDao;

	public CanalServiceImpl() {

	}

	@Override
	@Transactional
	public void save(Canal canal) {
		this.canalDao.save(canal);
	}

	public CanalDao getCanalDao() {
		return canalDao;
	}

	public void setCanalDao(CanalDao canalDao) {
		this.canalDao = canalDao;
	}

}
