package com.morgan.apps.card.service;

import java.util.List;

import com.morgan.apps.card.model.Alerta;

public interface AlertService {

	public void save(Alerta alerta);

	public List<Alerta> loadAll();

}
