package com.morgan.apps.card.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.morgan.apps.card.dao.AlertDao;
import com.morgan.apps.card.model.Alerta;
import com.morgan.apps.card.service.AlertService;

@Service("alertService")
public class AlertServiceImpl implements AlertService {

     @Autowired
     private AlertDao alertDao;

     @Override
     @Transactional
     public void save(Alerta alerta) {
          this.alertDao.save(alerta);

     }

     @Override
     @Transactional
     public List<Alerta> loadAll() {
          Map<String, Object> parametersNamesAndValues = new HashMap<>();

          return this.alertDao.createQuery("FROM Alerta ORDER BY fecha DESC",
                    parametersNamesAndValues, 10);
     }

     public AlertDao getAlertDao() {
          return alertDao;
     }

     public void setAlertDao(AlertDao alertDao) {
          this.alertDao = alertDao;
     }

}
