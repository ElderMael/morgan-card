package com.morgan.apps.card.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Medicion {

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     private Long id;

     @Basic
     private Double valor;

     @ManyToOne
     private Canal canal;

     @Temporal(TemporalType.TIMESTAMP)
     private Date fecha;

     public Medicion() {

     }

     public Medicion(Long id, Canal canal, Double valor, Date fecha) {
          this.id = id;
          this.valor = valor;
          this.fecha = fecha;
          this.canal = canal;
     }

     @Override
     public String toString() {
          return "Medicion: " + (this.valor * this.canal.getFactor()) + " "
                    + this.canal.getUnidades();
     };

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public Double getValor() {
          return valor;
     }

     public void setValor(Double valor) {
          this.valor = valor;
     }

     public Canal getCanal() {
          return canal;
     }

     public void setCanal(Canal canal) {
          this.canal = canal;
     }

     public Date getFecha() {
          return fecha;
     }

     public void setFecha(Date fecha) {
          this.fecha = fecha;
     }

}
