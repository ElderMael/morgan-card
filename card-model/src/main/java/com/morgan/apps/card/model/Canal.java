package com.morgan.apps.card.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Canal {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Basic
	private String nombre;

	@Basic
	private String unidades;

	@Basic
	private String descripcion;

	@Basic
	private Double factor;

	@ManyToOne
	private Motor motor;

	@OneToMany
	private List<Medicion> mediciones;

	public Canal() {

	}

	public Canal(Long id, String nombre, String unidades, String descripcion,
			Motor motor, List<Medicion> medidas) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.unidades = unidades;
		this.descripcion = descripcion;
		this.motor = motor;
		this.mediciones = medidas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUnidades() {
		return unidades;
	}

	public void setUnidades(String unidades) {
		this.unidades = unidades;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Motor getMotor() {
		return motor;
	}

	public void setMotor(Motor motor) {
		this.motor = motor;
	}

	public List<Medicion> getMediciones() {
		return mediciones;
	}

	public void setMediciones(List<Medicion> mediciones) {
		this.mediciones = mediciones;
	}

	public Double getFactor() {
		return factor;
	}

	public void setFactor(Double factor) {
		this.factor = factor;
	}

}
