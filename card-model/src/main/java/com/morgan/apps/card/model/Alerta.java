package com.morgan.apps.card.model;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Alerta {

	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;

	@Basic
	private Date fecha;

	@Basic
	private Double medicion;

	public Alerta() {

	}

	public Alerta(Long id, Date fecha, Double medicion) {
		super();
		this.id = id;
		this.fecha = fecha;
		this.medicion = medicion;
	}

	public Double getMedicion() {
		return medicion;
	}

	public void setMedicion(Double medicion) {
		this.medicion = medicion;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
