package com.morgan.apps.card.model;

import java.util.List;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Motor {

     @Id
     @GeneratedValue(strategy = GenerationType.TABLE)
     private Long id;

     @Basic
     private String name;

     @Basic
     private String descripcion;

     @OneToMany
     private List<Canal> canales;

     public Motor() {

     }

     public Motor(Long id, String name, List<Canal> canales) {
          this.id = id;
          this.name = name;
          this.canales = canales;
     }

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }

     public List<Canal> getCanales() {
          return canales;
     }

     public void setCanales(List<Canal> canales) {
          this.canales = canales;
     }

     public String getDescripcion() {
          return descripcion;
     }

     public void setDescripcion(String descripcion) {
          this.descripcion = descripcion;
     }

}
