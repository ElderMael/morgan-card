package com.morgan.apps.card.reader.listeners;

import java.util.Date;
import java.util.List;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.morgan.apps.card.model.Alerta;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.reader.AlertListener;
import com.morgan.apps.card.service.AlertService;
import com.morgan.apps.card.service.MedicionService;

public class OverFlowAlertListener implements AlertListener {

     @Autowired
     private AlertService alertService;

     @Autowired
     private MedicionService medicionService;

     private MailSender mailSender;

     private SimpleMailMessage templateMessage;

     private String defaultEmail;

     public OverFlowAlertListener() {

     }

     @Override
     public void sendAlert(Medicion medicion) {

          Alerta alerta = new Alerta(null, new Date(), medicion.getValor()
                    * medicion.getCanal().getFactor());

          this.alertService.save(alerta);

          sendMail(medicion);

     }

     private void sendMail(Medicion medicion) {
          SimpleMailMessage msg = new SimpleMailMessage(this.templateMessage);
          msg.setTo(this.defaultEmail);

          List<Medicion> ultimasMediciones = medicionService.loadLastTen();

          DescriptiveStatistics statistics = new DescriptiveStatistics();

          for (Medicion m : ultimasMediciones) {

               statistics.addValue(m.getValor() * m.getCanal().getFactor());

          }

          double data = medicion.getValor() * medicion.getCanal().getFactor();

          String.valueOf(data);

          msg.setText("Medicion repasa el limite: " + data
                    + " obtenida en fecha " + medicion.getFecha()
                    + " el promedio de las ultimas 10 mediciones es: "
                    + statistics.getMean() + " y el maximo es:"
                    + statistics.getMax());

          try {
               this.mailSender.send(msg);
          } catch (Exception e) {
               System.out.println("Error al enviar email");
          }
     }

     public AlertService getAlertService() {
          return alertService;
     }

     public void setAlertService(AlertService alertService) {
          this.alertService = alertService;
     }

     public MailSender getMailSender() {
          return mailSender;
     }

     public void setMailSender(MailSender mailSender) {
          this.mailSender = mailSender;
     }

     public SimpleMailMessage getTemplateMessage() {
          return templateMessage;
     }

     public void setTemplateMessage(SimpleMailMessage templateMessage) {
          this.templateMessage = templateMessage;
     }

     public String getDefaultEmail() {
          return defaultEmail;
     }

     public void setDefaultEmail(String defaultEmail) {
          this.defaultEmail = defaultEmail;
     }

}
