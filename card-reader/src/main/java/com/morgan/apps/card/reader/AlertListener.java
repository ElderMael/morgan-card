package com.morgan.apps.card.reader;

import com.morgan.apps.card.model.Medicion;

public interface AlertListener {

     public void sendAlert(Medicion medicion);

}
