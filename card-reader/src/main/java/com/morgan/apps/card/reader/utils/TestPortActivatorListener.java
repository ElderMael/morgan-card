package com.morgan.apps.card.reader.utils;

import java.util.LinkedList;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.model.Motor;
import com.morgan.apps.card.reader.App;
import com.morgan.apps.card.reader.DataListenerTemplate;
import com.morgan.apps.card.service.CanalService;
import com.morgan.apps.card.service.MotorService;

public class TestPortActivatorListener implements ServletContextListener {

	protected static final Logger log = LoggerFactory
			.getLogger(TestPortActivatorListener.class);

	public TestPortActivatorListener() {

	}

	@SuppressWarnings("unchecked")
	@Override
	public void contextInitialized(ServletContextEvent sce) {

		WebApplicationContext context = WebApplicationContextUtils
				.getRequiredWebApplicationContext(sce.getServletContext());

		DataListenerTemplate<Medicion> listener = context
				.getBean(DataListenerTemplate.class);

		Motor m = new Motor(null, "MotorA", new LinkedList<Canal>());

		context.getBean(MotorService.class).save(m);

		Canal c = new Canal(null, "Prensa", " kg", "Sensor de la prensa", m,
				new LinkedList<Medicion>());

		m.getCanales().add(c);

		context.getBean(CanalService.class).save(c);

		App.motor = m;

		App.canal = c;

		listener.start();

	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {

	}

}
