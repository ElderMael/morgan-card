package com.morgan.apps.card.reader.web.chart;

import java.util.Map;

import org.primefaces.model.chart.ChartSeries;

public class ScrollableChartSeries extends ChartSeries {

     public ScrollableChartSeries() {
          this(10);
     }

     public ScrollableChartSeries(int windowSize) {
          this.setData(new ScrollableLinkedHashMap<Object, Number>(windowSize));
     }

}
