package com.morgan.apps.card.reader.listeners;

import java.util.List;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.morgan.apps.card.model.Medicion;

import com.morgan.apps.card.reader.AlertListener;

import com.morgan.apps.card.reader.App;
import com.morgan.apps.card.reader.PortDataListener;
import com.morgan.apps.card.service.MedicionService;

public class SimpleDataListener implements PortDataListener<Medicion>,
          ApplicationContextAware {

     protected static final Logger log = LoggerFactory
               .getLogger(SimpleDataListener.class);

     @Autowired
     private MedicionService medicionService;

     private ApplicationContext context;

     private AlertListener alertListener;

     private int sendedAlerts = 0;

     private int maxAlerts = 3;

     public SimpleDataListener() {

     }

     @Override
     public void portDataReceived(List<Medicion> data) {

          if ( !data.isEmpty() ) {
               for (Medicion medicion : data) {

                    medicion.setCanal(App.canal);

                    this.medicionService.save(medicion);

                    log.debug("{} - ID: {}", medicion, medicion.getId());

                    if ( medicion.getValor() >= 818 ) {

                         if ( this.sendedAlerts < this.maxAlerts ) {

                              this.alertListener.sendAlert(medicion);

                              this.sendedAlerts++;
                         }
                    }

               }

               context.getBean(SessionFactory.class).getCurrentSession()
                         .flush();
          }

     }

     @Override
     public void setApplicationContext(ApplicationContext applicationContext)
               throws BeansException {

          this.context = applicationContext;

     }

     public AlertListener getAlertListener() {
          return alertListener;
     }

     public void setAlertListener(AlertListener alertListener) {
          this.alertListener = alertListener;
     }

}
