package com.morgan.apps.card.reader;

import java.io.InputStream;
import java.nio.ByteBuffer;

public interface PortDataReader {

     ByteBuffer readData(InputStream inputStream);

}
