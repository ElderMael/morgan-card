package com.morgan.apps.card.reader.web.conversation;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.context.spi.CurrentSessionContext;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.morgan.apps.card.reader.utils.JsfUtils;

public class ConversationalCurrentSessionContext implements CurrentSessionContext {

	private static final long serialVersionUID = 803157986557235023L;

	protected static final Logger log = LoggerFactory
			.getLogger(ConversationalCurrentSessionContext.class);

	public ConversationalCurrentSessionContext() {

	}

	public ConversationalCurrentSessionContext(SessionFactoryImplementor sessionFactoryImplementor) {
		ConversationManager.setFactory(sessionFactoryImplementor);
	}

	@Override
	public Session currentSession() throws HibernateException {

		HttpServletRequest request = null;

		try {
			request = JsfUtils.getCurrentHttpRequest();
		} catch (Exception e) {
			log.debug("No current faces context found, returning default conversation.");
		}

		if (request == null) {
			return (Session) ConversationManager.getDefaultConversationSession();
		} else {
			return (Session) ConversationManager.getSessionForRequest(request);
		}
	}
}
