package com.morgan.apps.card.reader;

import java.util.List;
import java.util.Map;

public interface DataParser {

     public List<Map<String, String>> parse(String data);

}
