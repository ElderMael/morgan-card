package com.morgan.apps.card.reader.utils;

/**
 * Interface donde se guardan las constantes utilizadas dentro del modulo web.
 * 
 * @author ElderMael
 * 
 */
public interface Constantes {

     // Conversation
     String ACTIVE_CONVERSATION_ATTRIBUTE_NAME = "mx.gob.jtjo.conversation.active";
     String ACTIVE_CONVERSATION_COOKIE_NAME = "mx.gob.jgtjo.mana.cookie";

     // Spring security
     String SPRING_SECURITY_LOGIN_URL = "/j_spring_security_check";
     String SPRING_SECURITY_PASSWORD_PARAMETER = "j_password";
     String SPRING_SECURITY_USERNAME_PARAMETER = "j_username";

     // JSF Messages
     String DEFAULT_CUSTOM_FACES_MESSAGE_BUNDLE_NAME = "mx.gob.jgtjo.apps.schedule.web.appmsgs";

}
