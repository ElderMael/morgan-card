package com.morgan.apps.card.reader;

import java.nio.ByteBuffer;

public interface PortDataDecoder {

     public String decode(ByteBuffer data);

}
