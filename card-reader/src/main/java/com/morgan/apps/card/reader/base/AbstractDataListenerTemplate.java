package com.morgan.apps.card.reader.base;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Map;

import com.morgan.apps.card.reader.DataConverter;
import com.morgan.apps.card.reader.DataListenerTemplate;
import com.morgan.apps.card.reader.DataParser;
import com.morgan.apps.card.reader.PortDataDecoder;
import com.morgan.apps.card.reader.PortDataListener;
import com.morgan.apps.card.reader.PortDataReader;

public abstract class AbstractDataListenerTemplate<T> implements
          DataListenerTemplate<T> {

     private PortDataReader reader;

     private PortDataDecoder decoder;

     private DataParser parser;

     private DataConverter<T> converter;

     private PortDataListener<T> listener;

     public void setReader(PortDataReader reader) {
          this.reader = reader;
     }

     public void setDecoder(PortDataDecoder decoder) {
          this.decoder = decoder;
     }

     public void setParser(DataParser parser) {
          this.parser = parser;
     }

     public void setConverter(DataConverter<T> converter) {
          this.converter = converter;
     }

     public void setListener(PortDataListener<T> portDataListener) {
          this.listener = portDataListener;
     }

     /**
      * Subclasses must implement this method when data from the abstract port
      * is avaliable and call
      * {@link AbstractDataListenerTemplate#notifyListeners(InputStream)}
      */
     public abstract void onNewPortData();

     public final void notifyListeners(InputStream inputStream) {
          ByteBuffer buffer = this.reader.readData(inputStream);

          String data = this.decoder.decode(buffer);

          if ( data.trim().isEmpty() ) // Not real data, just control chars
               return;

          List<Map<String, String>> values = this.parser.parse(data);

          List<T> model = this.converter.convert(values);
          

          // Notify listener
          this.listener.portDataReceived(model);

     }

     public PortDataReader getReader() {
          return reader;
     }

     public PortDataDecoder getDecoder() {
          return decoder;
     }

     public DataParser getParser() {
          return parser;
     }

     public DataConverter<T> getConverter() {
          return converter;
     }

}
