package com.morgan.apps.card.reader.rs232;

import java.io.IOException;
import java.util.Enumeration;
import java.util.TooManyListenersException;

import javax.comm.CommPortIdentifier;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.SerialPortEvent;
import javax.comm.SerialPortEventListener;
import javax.comm.UnsupportedCommOperationException;

import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.reader.base.AbstractDataListenerTemplate;

public class Rs232DataListenerTemplate extends
		AbstractDataListenerTemplate<Medicion> implements
		SerialPortEventListener, Runnable {

	private SerialPort serialPort;

	private String serialPortName;

	private Integer baudrate = 9600;
	private Integer dataBits = SerialPort.DATABITS_8;
	private Integer stopBits = SerialPort.STOPBITS_1;
	private Integer parity = SerialPort.PARITY_NONE;

	private Thread readThread;

	public Rs232DataListenerTemplate() {

	}

	public Rs232DataListenerTemplate(String portName) {
		this.serialPortName = portName;
	}

	@SuppressWarnings("rawtypes")
	public void start() {
		try {
			Enumeration portList = CommPortIdentifier.getPortIdentifiers();

			CommPortIdentifier portId;

			while (portList.hasMoreElements()) {
				portId = (CommPortIdentifier) portList.nextElement();

				if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
					if (portId.getName().equals(this.serialPortName)) {

						this.serialPort = (SerialPort) portId.open(
								"card-reader", 2000);

						this.serialPort.addEventListener(this);

						this.serialPort.notifyOnDataAvailable(true);

						this.serialPort.setSerialPortParams(9600,
								SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
								SerialPort.PARITY_NONE);

						this.readThread = new Thread(this);
						this.readThread.start();

					}
				}

			}
		} catch (PortInUseException e) {
			throw new RuntimeException("Port '" + this.serialPortName
					+ "' is in use", e);
		} catch (TooManyListenersException e) {
			throw new RuntimeException("Port '" + this.serialPortName
					+ "' has to many listeners", e);
		} catch (UnsupportedCommOperationException e) {
			throw new RuntimeException("Port '" + this.serialPortName
					+ "' does not support current configuration", e);
		}
	}

	public void serialEvent(SerialPortEvent event) {

		if (event.getEventType() == SerialPortEvent.DATA_AVAILABLE) {
			this.onNewPortData();
		}

	}

	@Override
	public void onNewPortData() {

		try {
			this.notifyListeners(this.serialPort.getInputStream());
		} catch (IOException e) {
			throw new RuntimeException("Cannot open stream to serial port", e);
		}

	}

	public void run() {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			throw new RuntimeException("Reading thread interrupted for", e);
		}

	}

	public SerialPort getSerialPort() {
		return serialPort;
	}

	public void setSerialPort(SerialPort serialPort) {
		this.serialPort = serialPort;
	}

	public String getSerialPortName() {
		return serialPortName;
	}

	public void setSerialPortName(String serialPortName) {
		this.serialPortName = serialPortName;
	}

	public Integer getBaudrate() {
		return baudrate;
	}

	public void setBaudrate(Integer baudrate) {
		this.baudrate = baudrate;
	}

	public Integer getDataBits() {
		return dataBits;
	}

	public void setDataBits(Integer dataBits) {
		this.dataBits = dataBits;
	}

	public Integer getStopBits() {
		return stopBits;
	}

	public void setStopBits(Integer stopBits) {
		this.stopBits = stopBits;
	}

	public Integer getParity() {
		return parity;
	}

	public void setParity(Integer parity) {
		this.parity = parity;
	}

}
