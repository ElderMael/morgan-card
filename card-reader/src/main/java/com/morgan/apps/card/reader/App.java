package com.morgan.apps.card.reader;

import java.util.LinkedList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.model.Motor;
import com.morgan.apps.card.service.CanalService;
import com.morgan.apps.card.service.MotorService;

public class App {

	public static Motor motor;

	public static Canal canal;

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		System.out.println("Initializing context");
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"com/morgan/apps/card/reader/applicationContext.xml");

		DataListenerTemplate<Medicion> listener = context
				.getBean(DataListenerTemplate.class);

		Motor m = new Motor(null, "MotorA", new LinkedList<Canal>());

		context.getBean(MotorService.class).save(m);

		Canal c = new Canal(null, "Prensa", " kg", "Sensor de la prensa", m,
				new LinkedList<Medicion>());

		m.getCanales().add(c);

		context.getBean(CanalService.class).save(c);

		motor = m;

		canal = c;

		listener.start();

	}
}
