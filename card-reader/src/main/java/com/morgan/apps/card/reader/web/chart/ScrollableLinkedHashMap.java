package com.morgan.apps.card.reader.web.chart;

import java.util.LinkedHashMap;

public class ScrollableLinkedHashMap<K, V> extends LinkedHashMap<K, V> {

     private int windowSize;

     public ScrollableLinkedHashMap() {
          this(10);
     }

     public ScrollableLinkedHashMap(int windowSize) {
          this.windowSize = 10;
     }

     @Override
     protected boolean removeEldestEntry(java.util.Map.Entry<K, V> eldest) {
          return this.size() > this.windowSize;
     }

}
