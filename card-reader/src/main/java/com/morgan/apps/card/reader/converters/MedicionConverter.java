package com.morgan.apps.card.reader.converters;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.reader.DataConverter;

public class MedicionConverter implements DataConverter<Medicion> {

	@Override
	public List<Medicion> convert(List<Map<String, String>> data) {

		List<Medicion> mediciones = new LinkedList<>();

		for (Map<String, String> map : data) {
			Medicion medicion = new Medicion();

			Double valor = Double.parseDouble(map.get("medida"));
			String canalId = map.get("canal");

			if (canalId.isEmpty())
				canalId = "a";

			Canal canal = loadCanal(canalId);

			medicion.setFecha(new Date());
			medicion.setValor(valor);
			medicion.setCanal(canal);

			mediciones.add(medicion);
		}

		return mediciones;
	}

	private Canal loadCanal(String canalId) {

		return new Canal(1L, "Canal Raro", "°C", "Canal de temperatura", null,
				new LinkedList<Medicion>());
	}

}
