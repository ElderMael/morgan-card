package com.morgan.apps.card.reader.rs232;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import com.morgan.apps.card.reader.PortDataReader;

public class Rs232DataReader implements PortDataReader {

     public ByteBuffer readData(InputStream inputStream) {

          try {

               ByteBuffer buffer = ByteBuffer.allocate(10);

               byte[] byteBuffer = new byte[1];

               boolean endOfLine = false;

               while (!endOfLine) {
                    inputStream.read(byteBuffer);

                    if ( byteBuffer[0] == 13 ) // ASCII for LF (Line Feed)
                         endOfLine = true;

                    buffer.put(byteBuffer[0]);
               }

               return buffer;

          } catch (IOException e) {
               throw new RuntimeException("Cannot read data from port", e);
          }
     }

}
