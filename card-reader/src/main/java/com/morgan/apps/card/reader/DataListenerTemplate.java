package com.morgan.apps.card.reader;

public interface DataListenerTemplate<T> {

     public void setReader(PortDataReader reader);

     public void setDecoder(PortDataDecoder decoder);

     public void setParser(DataParser parser);

     public void setConverter(DataConverter<T> converter);

     public void setListener(PortDataListener<T> portDataListener);

     public void start();

}
