package com.morgan.apps.card.reader.parsers;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.morgan.apps.card.reader.DataParser;

public class RegexDiscardParser implements DataParser {

     private Pattern pattern;

     public RegexDiscardParser() {
          this("(?<canal>[A-Za-z]?)(?<medida>\\d+)");
     }

     public RegexDiscardParser(String regex) {
          this.pattern = Pattern.compile(regex);
     }

     public List<Map<String, String>> parse(String data) {

          Matcher matcher = this.pattern.matcher(data);

          List<Map<String, String>> matches = new LinkedList<Map<String, String>>();

          while (matcher.find()) {

               Map<String, String> match = new HashMap<String, String>();

               match.put("canal", matcher.group("canal"));
               match.put("medida", matcher.group("medida"));

               matches.add(match);

          }

          return matches;
     }

}
