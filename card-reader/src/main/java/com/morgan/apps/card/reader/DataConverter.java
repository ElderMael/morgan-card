package com.morgan.apps.card.reader;

import java.util.List;
import java.util.Map;

public interface DataConverter<T> {

     public List<T> convert(List<Map<String, String>> data);

}
