package com.morgan.apps.card.reader.decoders;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import com.morgan.apps.card.reader.PortDataDecoder;

public class AsciiPortDataDecoder implements PortDataDecoder {

     public String decode(ByteBuffer data) {

          try {
               return new String(data.array(), "US-ASCII");
          } catch (UnsupportedEncodingException e) {
               throw new RuntimeException(
                         "Cannot decode bytes to String in US-ASCII encoding",
                         e);
          }
     }

}
