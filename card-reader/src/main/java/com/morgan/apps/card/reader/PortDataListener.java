package com.morgan.apps.card.reader;

import java.util.List;

public interface PortDataListener<T> {

     public void portDataReceived(List<T> data);

}
