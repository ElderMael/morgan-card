package com.morgan.apps.card.reader.web.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import com.morgan.apps.card.model.Alerta;
import com.morgan.apps.card.model.Canal;
import com.morgan.apps.card.model.Medicion;
import com.morgan.apps.card.model.Motor;
import com.morgan.apps.card.reader.App;
import com.morgan.apps.card.reader.DataListenerTemplate;
import com.morgan.apps.card.reader.utils.JsfUtils;
import com.morgan.apps.card.reader.web.chart.ScrollableChartSeries;
import com.morgan.apps.card.reader.web.conversation.ConversationManager;
import com.morgan.apps.card.service.AlertService;
import com.morgan.apps.card.service.CanalService;
import com.morgan.apps.card.service.MedicionService;
import com.morgan.apps.card.service.MotorService;

@ManagedBean(name = "startBean")
@SessionScoped
public class StartBean {

	public static final int WINDOW_SIZE = 10;

	protected static final Logger log = LoggerFactory
			.getLogger(StartBean.class);

	@ManagedProperty(value = "#{medicionService}")
	private MedicionService medicionService;

	@ManagedProperty(value = "#{alertService}")
	private AlertService alertService;

	private List<Alerta> alertas = new LinkedList<>();

	private CartesianChartModel chartModel = new CartesianChartModel();

	private ChartSeries promedio;

	private ChartSeries maximo;

	private ChartSeries actual;

	private TimeZone timeZone = TimeZone.getTimeZone("GMT-6:00");

	private Locale locale = new Locale("es", "MX");

	private DateFormat format;

	private DescriptiveStatistics descriptiveStatistics = new DescriptiveStatistics();

	public StartBean() {

		this.promedio = new ScrollableChartSeries(WINDOW_SIZE);
		this.maximo = new ScrollableChartSeries(WINDOW_SIZE);
		this.actual = new ScrollableChartSeries(WINDOW_SIZE);

		this.promedio.setLabel("Promedio");
		this.maximo.setLabel("Maximo");
		this.actual.setLabel("Actual");

		this.chartModel.addSeries(this.promedio);
		this.chartModel.addSeries(this.maximo);
		this.chartModel.addSeries(this.actual);

		this.format = new SimpleDateFormat("HH:mm:ss");

		this.format.setTimeZone(timeZone);

		this.actual.set(this.format.format(new Date()), 0.0);
		this.maximo.set(this.format.format(new Date()), 0.0);
		this.promedio.set(this.format.format(new Date()), 0.0);

	}

	public void updateAlertas() {
		this.alertas = this.alertService.loadAll();
	}

	public void updateChart() {

		List<Medicion> mediciones = this.medicionService.loadLastTen();

		for (Medicion m : mediciones) {

			String hora = this.format.format(m.getFecha());

			Double medicion = m.getValor() * m.getCanal().getFactor();

			this.descriptiveStatistics.addValue(medicion);

			this.actual.set(hora, medicion);

			this.maximo.set(hora, this.descriptiveStatistics.getMax());

			this.promedio.set(hora, this.descriptiveStatistics.getMean());

		}

		System.out.println("PPR?");

	}

	@SuppressWarnings("unchecked")
	public void start() {

		WebApplicationContext context = FacesContextUtils
				.getRequiredWebApplicationContext(FacesContext
						.getCurrentInstance());

		DataListenerTemplate<Medicion> listener = context
				.getBean(DataListenerTemplate.class);

		Motor m = new Motor(null, "MotorA", new LinkedList<Canal>());

		context.getBean(MotorService.class).save(m);

		Canal c = new Canal(null, "Prensa", "toneladas", "Sensor de la prensa",
				m, new LinkedList<Medicion>());
		c.setFactor(12.21);

		m.getCanales().add(c);

		context.getBean(CanalService.class).save(c);

		App.motor = m;

		App.canal = c;

		ConversationManager.getSessionForRequest(
				JsfUtils.getCurrentHttpRequest()).flush();

		listener.start();

	}

	public CartesianChartModel getChartModel() {
		return chartModel;
	}

	public void setChartModel(CartesianChartModel chartModel) {
		this.chartModel = chartModel;
	}

	public MedicionService getMedicionService() {
		return medicionService;
	}

	public void setMedicionService(MedicionService medicionService) {
		this.medicionService = medicionService;
	}

	public List<Alerta> getAlertas() {
		return alertas;
	}

	public void setAlertas(List<Alerta> alertas) {
		this.alertas = alertas;
	}

	public AlertService getAlertService() {
		return alertService;
	}

	public void setAlertService(AlertService alertService) {
		this.alertService = alertService;
	}

	public Double getNumeroActual() {

		if (this.descriptiveStatistics.getN() == 0) {
			return 0.0;
		}
		return this.descriptiveStatistics
				.getElement((int) (this.descriptiveStatistics.getN() - 1));
	}

	public Double getNumeroPromedio() {
		if (this.descriptiveStatistics.getN() == 0) {
			return 0.0;
		}
		return this.descriptiveStatistics.getMean();
	}

	public Double getNumeroMaximo() {
		if (this.descriptiveStatistics.getN() == 0) {
			return 0.0;
		}
		return this.descriptiveStatistics.getMax();
	}

}
