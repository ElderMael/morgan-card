package com.morgan.apps.card.dao;

import com.morgan.apps.card.model.Motor;

public interface MotorDao extends Dao<Long, Motor> {

}
