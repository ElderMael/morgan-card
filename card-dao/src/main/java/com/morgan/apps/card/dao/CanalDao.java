package com.morgan.apps.card.dao;

import com.morgan.apps.card.model.Canal;

public interface CanalDao extends Dao<Long, Canal> {

}
