package com.morgan.apps.card.dao.hibernate;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.morgan.apps.card.dao.Dao;

@SuppressWarnings("unchecked")
public class HibernateBaseDao<Pk extends Serializable, T> implements Dao<Pk, T> {

     @Autowired
     private SessionFactory sessionFactory;

     private Class<T> type;

     public HibernateBaseDao() {

     }

     public Class<T> getType() {

          if ( this.type == null ) {

               ParameterizedType parameterizedType = (ParameterizedType) (this
                         .getClass().getGenericSuperclass());

               while (!(parameterizedType instanceof ParameterizedType)) {
                    parameterizedType = (ParameterizedType) parameterizedType
                              .getClass().getGenericSuperclass();
               }

               this.type = (Class<T>) parameterizedType
                         .getActualTypeArguments()[0];

          }

          return this.type;
     }

     public SessionFactory getSessionFactory() {
          return sessionFactory;
     }

     public void setSessionFactory(SessionFactory sessionFactory) {
          this.sessionFactory = sessionFactory;
     }

     public T get(Pk id) {
          return (T) this.sessionFactory.getCurrentSession().get(
                    this.getType(), id);
     }

     public T load(Pk id) {
          return (T) this.sessionFactory.getCurrentSession().load(
                    this.getType(), id);
     }

     public T merge(T persistedObject) {
          return (T) this.sessionFactory.getCurrentSession().merge(
                    persistedObject);
     }

     public void persist(T transientObject) {
          this.sessionFactory.getCurrentSession().persist(transientObject);

     }

     public void refresh(T persistedObject) {
          this.sessionFactory.getCurrentSession().refresh(persistedObject);
     }

     public Pk save(T transientObject) {
          return (Pk) this.sessionFactory.getCurrentSession().save(
                    transientObject);
     }

     public void update(T persistedObject) {
          this.sessionFactory.getCurrentSession().update(persistedObject);
     }

     public void saveOrUpdate(T object) {
          this.sessionFactory.getCurrentSession().saveOrUpdate(object);
     }

     public List<T> createQuery(String ormQuery,
               Map<String, Object> parametersNamesAndValues) {
          Query query = this.sessionFactory.getCurrentSession().createQuery(
                    ormQuery);

          for (Entry<String, Object> entry : parametersNamesAndValues
                    .entrySet()) {
               query.setParameter(entry.getKey(), entry.getValue());

          }

          return query.list();
     }

     @Override
     public List<T> createQuery(String ormQuery,
               Map<String, Object> parametersNamesAndValues, int maxResults) {
          Query query = this.sessionFactory.getCurrentSession().createQuery(
                    ormQuery);

          for (Entry<String, Object> entry : parametersNamesAndValues
                    .entrySet()) {
               query.setParameter(entry.getKey(), entry.getValue());

          }

          query.setMaxResults(maxResults);
          return query.list();
     }

}
