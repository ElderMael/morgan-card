package com.morgan.apps.card.dao;

import com.morgan.apps.card.model.Alerta;

public interface AlertDao extends Dao<Long, Alerta> {

}
