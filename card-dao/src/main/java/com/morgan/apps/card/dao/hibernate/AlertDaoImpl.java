package com.morgan.apps.card.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.morgan.apps.card.dao.AlertDao;
import com.morgan.apps.card.model.Alerta;

@Repository("alertDao")
public class AlertDaoImpl extends HibernateBaseDao<Long, Alerta> implements
		AlertDao {

}
