package com.morgan.apps.card.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.morgan.apps.card.dao.MedicionDao;
import com.morgan.apps.card.model.Medicion;

@Repository("medicionDao")
public class MedicionDaoimpl extends HibernateBaseDao<Long, Medicion> implements
          MedicionDao {

}
