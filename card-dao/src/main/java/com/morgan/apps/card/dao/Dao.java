package com.morgan.apps.card.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface Dao<Pk extends Serializable, T> {

     public T get(Pk id);

     public T load(Pk id);

     public T merge(T persistedObject);

     public void persist(T transientObject);

     public void refresh(T persistedObject);

     public Pk save(T transientObject);

     public void update(T persistedObject);

     public void saveOrUpdate(T object);

     public List<T> createQuery(String ormQuery,
               Map<String, Object> parametersNamesAndValues);

     public List<T> createQuery(String ormQuery,
               Map<String, Object> parametersNamesAndValues, int maxResults);

}