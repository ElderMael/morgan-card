package com.morgan.apps.card.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.morgan.apps.card.dao.CanalDao;
import com.morgan.apps.card.model.Canal;

@Repository("canalDao")
public class CanalDaoImpl extends HibernateBaseDao<Long, Canal> implements
          CanalDao {

}
