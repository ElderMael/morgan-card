package com.morgan.apps.card.dao;

import com.morgan.apps.card.model.Medicion;

public interface MedicionDao extends Dao<Long, Medicion> {

}
