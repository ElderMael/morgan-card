package com.morgan.apps.card.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.morgan.apps.card.dao.MotorDao;
import com.morgan.apps.card.model.Motor;

@Repository("motorDao")
public class MotorDaoImpl extends HibernateBaseDao<Long, Motor> implements
          MotorDao {

}
